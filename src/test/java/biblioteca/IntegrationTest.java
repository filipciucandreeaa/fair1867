package biblioteca;

import org.junit.Test;

public class IntegrationTest {
    BigBangIntegrationTest bigBangIntegrationTest = new BigBangIntegrationTest();
    TopDownIntegrationTest topDownIntegrationTest = new TopDownIntegrationTest();

    @Test
    public void bigBangIntegrationTest(){
        bigBangIntegrationTest.unitA();
        bigBangIntegrationTest.unitB();
        bigBangIntegrationTest.unitC();
        bigBangIntegrationTest.integration();
        bigBangIntegrationTest.after();
    }


    @Test
    public void topDownIntegrationTest(){
        topDownIntegrationTest.unitA();
        topDownIntegrationTest.integrationB();
        topDownIntegrationTest.integrationC();
        topDownIntegrationTest.after();
    }
}
