package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TopDownIntegrationTest {
    BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo("cartiBDTDint.dat"));

    @Test
    public void unitA() {
        Result result = JUnitCore.runClasses(Funct1Test.class);
        assertEquals(result.getFailureCount(), 0);
    }

    @Test
    public void integrationB() {
        CartiRepoMock repoMock = new CartiRepoMock();

        //A
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cuvantcheie");
        List<String> referenti = new ArrayList<String>();
        referenti.add("mihai");
        Carte carte = Funct1Test.createBook("Luceafarul", "editura", "1995", cuvinteCheie, referenti);
        int oldsize = 0;
        try {
            oldsize = ctrl.getCarti().size();
            ctrl.adaugaCarte(carte);
            assertEquals(ctrl.getCarti().size(), oldsize + 1);
        } catch (Exception e) {
            assert (false);
        }

        //B
        final List<Carte> carti;
        try {
            carti = ctrl.cautaCarte("mihai");
            assert (carti.size() == 1);
            assertEquals(carti.get(0).getTitlu(), "Luceafarul");
        } catch (Exception e) {
            assert (false);
            e.printStackTrace();
        }

        //C
        List<Carte> cartiOrdonateDinAnul = repoMock.getCartiOrdonateDinAnul("1993");
        assert (cartiOrdonateDinAnul.size() == 0);
    }

    @Test
    public void integrationC() {
        // A
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cuvantcheie");
        List<String> referenti = new ArrayList<String>();
        referenti.add("Cristi");
        Carte carte = Funct1Test.createBook("ceva", "editura", "1993", cuvinteCheie, referenti);
        int oldsize = 0;
        try {
            oldsize = ctrl.getCarti().size();
            ctrl.adaugaCarte(carte);
            assertEquals(ctrl.getCarti().size(), oldsize + 1);
        } catch (Exception e) {
            assert (false);
        }

        //B
        final List<Carte> carti;
        try {
            carti = ctrl.cautaCarte("cristi");
            assert (carti.size() >= 1);
            assertEquals(carti.get(0).getTitlu(), "ceva");
        } catch (Exception e) {
            assert (false);
            e.printStackTrace();
        }

        //C
        try {
            List<Carte> cartiOrdonateDinAnul = ctrl.getCartiOrdonateDinAnul("1993");
            assert (cartiOrdonateDinAnul.size() == 1);
            assert (true);
        } catch (Exception e) {
            assert (false);
        }
    }

    @After
    public void after(){
        File file = new File("cartiBDTDint.dat");

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(file);
            writer.print("");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
