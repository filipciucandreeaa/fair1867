package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.AfterClass;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import static org.junit.Assert.assertEquals;

public class BigBangIntegrationTest {
    @Test
    public void unitA() {
        Result result = JUnitCore.runClasses(Funct1Test.class);
        assertEquals(result.getFailureCount(),0);
    }

    @Test
    public void unitB() {
        Result result = JUnitCore.runClasses(Funct2Test.class);
        assertEquals(result.getFailureCount(),0);
    }

    @Test
    public void unitC() {
        Result result = JUnitCore.runClasses(Funct3Test.class);
        assertEquals(result.getFailureCount(),0);
    }


    @Test
    public void integration() {
        BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo("cartiBDBBint.dat"));
        // A
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cuvantcheie");
        List<String> referenti = new ArrayList<String>();
        referenti.add("Mihai Eminescu");
        Carte carte = Funct1Test.createBook("Luceafarul", "editura", "1993", cuvinteCheie, referenti);
        int oldsize = 0;
        try {
            oldsize = ctrl.getCarti().size();
            ctrl.adaugaCarte(carte);
            assertEquals(ctrl.getCarti().size(), oldsize + 1);
        } catch (Exception e) {
            assert (false);
            e.printStackTrace();
        }

        //B
        final List<Carte> carti;
        try {
            carti = ctrl.cautaCarte("mihai");
            assert (carti.size() == 1);
        } catch (Exception e) {
            assert (false);
            e.printStackTrace();
        }

        //C
        try {
            List<Carte> cartiOrdonateDinAnul = ctrl.getCartiOrdonateDinAnul("1993");
            assert (cartiOrdonateDinAnul.size() == 1);
        } catch (Exception e) {
            assert (false);
        }
    }


    @AfterClass
    public static void after(){
        File file = new File("cartiBDBBint.dat");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(file);
            writer.print("");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}
