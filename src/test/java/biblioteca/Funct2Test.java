package biblioteca;

import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class Funct2Test {
    @Test
    public void cautaCarte1() throws Exception {
        CartiRepo repo  = new CartiRepo("./cartiBD1.dat");
        final List<Carte> carti = repo.cautaCarte("mihai");
        assertEquals(carti.size(),1);
        assertEquals(carti.get(0).getTitlu(),"Luceafarul");
    }

    @Test
    public void cautaCarte2() throws Exception {
        CartiRepo repo  = new CartiRepo("cartiBD2.dat");
        final List<Carte> carti = repo.cautaCarte("Mihai");
        assertEquals(carti.size(),1);
        assertEquals(carti.get(0).getTitlu(),"Luceafarul");
    }

    @Test
    public void cautaCarte3() throws Exception {
        CartiRepo repo  = new CartiRepo("cartiBD3.dat");
        final List<Carte> carti = repo.cautaCarte("Mihai");
        assertEquals(carti.size(),0);
    }

    @Test
    public void cautaCarte4() throws Exception {
        CartiRepo repo  = new CartiRepo("cartiBD4.dat");
        final List<Carte> carti = repo.cautaCarte("Mihai");
        assertEquals(carti.size(),0);
    }

    @Test
    public void cautaCarte5() throws Exception {
        CartiRepo repo  = new CartiRepo("cartiBD2.dat");
        final List<Carte> carti = repo.cautaCarte("Costel");
        assertEquals(carti.size(),0);
    }
}