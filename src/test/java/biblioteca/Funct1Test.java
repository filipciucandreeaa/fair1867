package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Funct1Test {
    BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo("cartiBD.dat"));

    @BeforeClass
    public static void before() {
        System.out.println("funct1 before");
        CartiRepo repo = new CartiRepo("cartiBD.dat");
        for(int i=0;i<5;i++) {
            List<String> cuvinteCheie = new ArrayList<String>();
            cuvinteCheie.add("cuvantcheie"+i);
            List<String> referenti = new ArrayList<String>();
            referenti.add("referent"+i);
            repo.adaugaCarte(createBook("", "editura"+i, "1993", cuvinteCheie, referenti));
        }
    }

    @AfterClass
    public static void after(){
        System.out.println("funct1 after");
        File file = new File("cartiBD.dat");

        if(file.delete()){
            System.out.println(file.getName() + " is deleted!");
        }else{
            System.out.println("Delete operation is failed.");
        }
    }

    @Test
    public void adaugaCarte1() throws Exception {
        // titlu invalid
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cuvantcheie");
        List<String> referenti = new ArrayList<String>();
        referenti.add("referent");
        Carte carte = createBook("", "editura", "1996", cuvinteCheie, referenti);
        try {
            ctrl.adaugaCarte(carte);
            assert (false);
        } catch (Exception e) {
            assert (true);
        }
    }

    @Test
    public void adaugaCarte2() {
        // all good
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cuvantcheie");
        List<String> referenti = new ArrayList<String>();
        referenti.add("referent");
        Carte carte = createBook("titlu", "editura", "1993", cuvinteCheie, referenti);
        int oldSize = 0;
        try {
            oldSize = ctrl.getCarti().size();
            ctrl.adaugaCarte(carte);
            assertEquals(ctrl.getCarti().size(), oldSize + 1);
        } catch (Exception e) {
            assert (false);
        }
    }

    @Test
    public void adaugaCarte3() {
        // editura invalida
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cuvantcheie");
        List<String> referenti = new ArrayList<String>();
        referenti.add("referent");
        Carte carte = createBook("titlu", "editurs122", "1993", cuvinteCheie, referenti);
        try {
            ctrl.adaugaCarte(carte);
            assert (false);
        } catch (Exception e) {
            assert (true);
        }
    }


    @Test
    public void adaugaCarte4() {
        // autor invalid
        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add("cuvantcheie");
        List<String> referenti = new ArrayList<String>();
        referenti.add("");
        Carte carte = createBook("titlu", "editurs", "1993", cuvinteCheie, referenti);
        try {
            ctrl.adaugaCarte(carte);
            assert (false);
        } catch (Exception e) {
            assert (true);
        }
    }

    public static Carte createBook(String titlu, String editura, String anAparitie, List<String> cuvinteCheie, List<String> referenti) {
        Carte carte = new Carte();
        carte.setTitlu(titlu);
        carte.setAnAparitie(anAparitie);
        carte.setEditura(editura);
        carte.setCuvinteCheie(cuvinteCheie);
        carte.setReferenti(referenti);
        return carte;
    }
}