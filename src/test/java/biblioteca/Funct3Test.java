package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.Test;

import java.util.List;

public class Funct3Test {
    BibliotecaCtrl ctrl = new BibliotecaCtrl(new CartiRepo("cartiDBFunct3.dat"));

    @Test
    public void getCartiOrdonateDinAnulValid() {
        // an valid
        try {
            List<Carte> carti = ctrl.getCartiOrdonateDinAnul("1993");
            assert (carti.size() == 5);
        } catch (Exception e) {
            assert (false);
        }
    }

    @Test
    public void getCartiOrdonateDinAnulInvalid() {
        // an invalid
        try {
            List<Carte> carti = ctrl.getCartiOrdonateDinAnul("1an02");
            assert (false);
        } catch (Exception e) {
            assert (true);
        }
    }
}
